import React, { useEffect, memo, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import get from 'lodash/get';
import debounce from 'lodash/debounce';
import isEmpty from 'lodash/isEmpty';
import { Card, Skeleton, Input, List, Avatar } from 'antd';
import styled from 'styled-components';
import { injectIntl } from 'react-intl';
import T from '@components/T';
import { useInjectSaga } from 'utils/injectSaga';
import {
  selectHomeContainer,
  selectSongsData,
  selectSongsError,
  selectArtistName
} from './selectors';
import { homeContainerCreators } from './reducer';
import saga from './saga';

const { Search } = Input;

const CustomCard = styled(Card)`
  && {
    margin: 20px 0;
    max-width: ${props => props.maxwidth}px;
    min-height: ${props => props.minheight}px;
    color: ${props => props.color};
    ${props => props.color && `color: ${props.color}`};
  }
`;
const Container = styled.div`
  && {
    display: flex;
    flex-direction: column;
    width: 100%;
    margin: 0 auto;
    padding: ${props => props.padding}px;
  }
`;
const CenterContainer = styled(Container)`
  && {
    align-items: center;
    max-width: 700px;
  }
`;
export function HomeContainer({
  dispatchITunesSongs,
  dispatchClearITunesSongs,
  intl,
  songsData = {},
  songsError = null,
  artistName,
  maxwidth,
  padding,
  listItemMinHieght
}) {
  useInjectSaga({ key: 'homeContainer', saga });
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const loaded = get(songsData, 'results', null) || songsError;
    if (loading && loaded) {
      setLoading(false);
    }
  }, [songsData]);

  const handleOnChange = rName => {
    if (!isEmpty(rName)) {
      dispatchITunesSongs(rName);
      setLoading(true);
    } else {
      dispatchClearITunesSongs();
    }
  };
  const debouncedHandleOnChange = debounce(handleOnChange, 200);

  const renderList = () => {
    const items = get(songsData, 'results', []);
    const totalCount = get(songsData, 'resultCount', 0);
    return (
      (items.length !== 0 || loading) && (
        <CustomCard>
          <Skeleton loading={loading} active>
            {artistName && (
              <div>
                <T id="search_query" values={{ artistName }} />
              </div>
            )}
            {totalCount !== 0 && (
              <div>
                <T id="matching_songs" values={{ totalCount }} />
              </div>
            )}
            <List
              grid={{ gutter: 16, column: 2 }}
              dataSource={items}
              renderItem={item => (
                <List.Item>
                  <CustomCard minheight={listItemMinHieght}>
                    <List.Item.Meta
                      avatar={
                        <Avatar
                          src={item.artworkUrl100}
                          shape="square"
                          size="large"
                        />
                      }
                      title={item.collectionName}
                      description={item.artistName}
                    />
                  </CustomCard>
                </List.Item>
              )}
            />
            ,
          </Skeleton>
        </CustomCard>
      )
    );
  };
  const renderErrorState = () => {
    let repoError;
    if (songsError) {
      repoError = songsError;
    } else if (!artistName && !get(songsData, 'resultCount', 0)) {
      repoError = 'songs_search_default';
    } else if (artistName && !get(songsData, 'resultCount', 0)) {
      repoError = 'no_result_found';
    }
    return (
      !loading &&
      repoError && (
        <CustomCard
          color={songsError ? 'red' : 'grey'}
          title={intl.formatMessage({ id: 'song_list' })}
        >
          <T id={repoError} />
        </CustomCard>
      )
    );
  };
  return (
    <Container padding={padding}>
      <CenterContainer maxwidth={maxwidth}>
        <CustomCard
          title={intl.formatMessage({ id: 'song_search' })}
          maxwidth={maxwidth}
        >
          <Search
            data-testid="search-bar"
            defaultValue={artistName}
            type="text"
            onChange={evt => debouncedHandleOnChange(evt.target.value)}
            onSearch={searchText => debouncedHandleOnChange(searchText)}
          />
        </CustomCard>
      </CenterContainer>
      {renderList()}
      {renderErrorState()}
    </Container>
  );
}

HomeContainer.propTypes = {
  dispatchITunesSongs: PropTypes.func,
  dispatchClearITunesSongs: PropTypes.func,
  intl: PropTypes.object,
  songsData: PropTypes.shape({
    resultCount: PropTypes.number,
    results: PropTypes.array
  }),
  songsError: PropTypes.object,
  artistName: PropTypes.string,
  history: PropTypes.object,
  maxwidth: PropTypes.number,
  padding: PropTypes.number,
  listItemMinHieght: PropTypes.number
};

HomeContainer.defaultProps = {
  maxwidth: 500,
  padding: 20,
  listItemMinHieght: 124
};

const mapStateToProps = createStructuredSelector({
  homeContainer: selectHomeContainer(),
  songsData: selectSongsData(),
  songsError: selectSongsError(),
  artistName: selectArtistName()
});

function mapDispatchToProps(dispatch) {
  const { requestGetITunesSongs, clearITunesSongs } = homeContainerCreators;
  return {
    dispatchITunesSongs: artistName =>
      dispatch(requestGetITunesSongs(artistName)),
    dispatchClearITunesSongs: () => dispatch(clearITunesSongs())
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default compose(
  injectIntl,
  withConnect,
  memo
)(HomeContainer);

export const HomeContainerTest = compose(injectIntl)(HomeContainer);
