/*
 *
 * HomeContainer reducer
 *
 */
import produce from 'immer';
import { fromJS } from 'immutable';
import { createActions } from 'reduxsauce';
import _ from 'lodash';

export const {
  Types: homeContainerTypes,
  Creators: homeContainerCreators
} = createActions({
  requestGetITunesSongs: ['artistName'],
  successGetITunesSongs: ['data'],
  failureGetITunesSongs: ['error'],
  clearITunesSongs: []
});
export const initialState = fromJS({});

/* eslint-disable default-case, no-param-reassign */
export const homeContainerReducer = (state = initialState, action) =>
  produce(state, (/* draft */) => {
    switch (action.type) {
      case homeContainerTypes.REQUEST_GET_I_TUNES_SONGS:
        return initialState.set('artistName', action.artistName);
      case homeContainerTypes.CLEAR_I_TUNES_SONGS:
        return initialState.set('artistName', null).set('songsData', null);
      case homeContainerTypes.SUCCESS_GET_I_TUNES_SONGS:
        return state.set('songsData', action.data);
      case homeContainerTypes.FAILURE_GET_I_TUNES_SONGS:
        return state.set(
          'songsError',
          _.get(action.error, 'message', 'something_went_wrong')
        );
    }
    return state;
  });

export default homeContainerReducer;
