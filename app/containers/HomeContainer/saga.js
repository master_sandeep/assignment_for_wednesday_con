import { put, call, takeLatest } from 'redux-saga/effects';
import { getSongs } from '@app/services/songsApi';
import { homeContainerTypes, homeContainerCreators } from './reducer';

const { REQUEST_GET_I_TUNES_SONGS } = homeContainerTypes;
const { successGetITunesSongs, failureGetITunesSongs } = homeContainerCreators;

export function* getITunesSongs(action) {
  const response = yield call(getSongs, action.artistName);
  const { data, ok } = response;
  if (ok) {
    yield put(successGetITunesSongs(data));
  } else {
    yield put(failureGetITunesSongs(data));
  }
}
// Individual exports for testing
export default function* homeContainerSaga() {
  yield takeLatest(REQUEST_GET_I_TUNES_SONGS, getITunesSongs);
}
