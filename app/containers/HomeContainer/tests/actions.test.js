import { homeContainerTypes, homeContainerCreators } from '../reducer';

describe('HomeContainer action tests', () => {
  it('has a type of REQUEST_GET_I_TUNES_SONGS', () => {
    const expected = {
      type: homeContainerTypes.REQUEST_GET_I_TUNES_SONGS,
      artistName: 'artistName'
    };
    expect(homeContainerCreators.requestGetITunesSongs('artistName')).toEqual(
      expected
    );
  });
});
