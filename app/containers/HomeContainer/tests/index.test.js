/**
 *
 * Tests for HomeContainer
 *
 */

import React from 'react';
import { timeout, renderProvider } from '@utils/testUtils';
import { fireEvent } from '@testing-library/dom';
import { HomeContainerTest as HomeContainer } from '../index';

describe('<HomeContainer /> tests', () => {
  let submitSpy;

  beforeEach(() => {
    submitSpy = jest.fn();
  });
  it('should render and match the snapshot', () => {
    const { baseElement } = renderProvider(
      <HomeContainer dispatchITunesSongs={submitSpy} />
    );
    expect(baseElement).toMatchSnapshot();
  });

  it('should call dispatchClearITunesSongs on empty change', async () => {
    const getITuneSongsSpy = jest.fn();
    const clearITuneSongsSpy = jest.fn();
    const { getByTestId } = renderProvider(
      <HomeContainer
        dispatchClearITunesSongs={clearITuneSongsSpy}
        dispatchITunesSongs={getITuneSongsSpy}
      />
    );
    fireEvent.change(getByTestId('search-bar'), {
      target: { value: 'a' }
    });
    await timeout(500);
    expect(getITuneSongsSpy).toBeCalled();
    fireEvent.change(getByTestId('search-bar'), {
      target: { value: '' }
    });
    await timeout(500);
    expect(clearITuneSongsSpy).toBeCalled();
  });

  it('should call dispatchITunesSongs on change', async () => {
    const { getByTestId } = renderProvider(
      <HomeContainer dispatchITunesSongs={submitSpy} />
    );
    fireEvent.change(getByTestId('search-bar'), {
      target: { value: 'some songs' }
    });
    await timeout(500);
    expect(submitSpy).toBeCalled();
  });
});
