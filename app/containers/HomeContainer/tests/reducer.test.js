import {
  homeContainerReducer,
  initialState,
  homeContainerTypes
} from '../reducer';

/* eslint-disable default-case, no-param-reassign */
describe('HomContainer reducer tests', () => {
  let state;
  beforeEach(() => {
    state = initialState;
  });

  it('should return the initial state', () => {
    expect(homeContainerReducer(undefined, {})).toEqual(state);
  });

  it('should return the initial state when an action of type REQUEST_GET_I_TUNES_SONGS is dispatched', () => {
    const artistName = 'Sonu';
    const expectedResult = state.set('artistName', artistName);
    expect(
      homeContainerReducer(state, {
        type: homeContainerTypes.REQUEST_GET_I_TUNES_SONGS,
        artistName
      })
    ).toEqual(expectedResult);
  });

  it('should ensure that the search data is present when SUCCESS_GET_I_TUNES_SONGS is dispatched', () => {
    const data = { name: 'Tere Bina' };
    const expectedResult = state.set('songsData', data);
    expect(
      homeContainerReducer(state, {
        type: homeContainerTypes.SUCCESS_GET_I_TUNES_SONGS,
        data
      })
    ).toEqual(expectedResult);
  });

  it('should ensure that the songsError has some data FAILURE_GET_I_TUNES_SONGS is dispatched', () => {
    const error = 'something_went_wrong';
    const expectedResult = state.set('songsError', error);
    expect(
      homeContainerReducer(state, {
        type: homeContainerTypes.FAILURE_GET_I_TUNES_SONGS,
        error
      })
    ).toEqual(expectedResult);
  });

  it('should ensure that the artistName and  songsData has null data when CLEAR_I_TUNES_SONGS is dispatched', () => {
    const error = 'something_went_wrong';
    const expectedResult = state.set('songsError', error);
    expect(
      homeContainerReducer(state, {
        type: homeContainerTypes.FAILURE_GET_I_TUNES_SONGS,
        error
      })
    ).toEqual(expectedResult);
  });
});
