/**
 * Test homeContainer sagas
 */

/* eslint-disable redux-saga/yield-effects */
import { takeLatest, call, put } from 'redux-saga/effects';
import { getSongs } from '@app/services/songsApi';
import { apiResponseGenerator } from '@utils/testUtils';
import homeContainerSaga, { getITunesSongs } from '../saga';
import { homeContainerTypes } from '../reducer';

describe('HomeContainer saga tests', () => {
  const generator = homeContainerSaga();
  const artistName = 'sonu';
  let getITunesSongsGenerator = getITunesSongs({ artistName });

  it('should start task to watch for REQUEST_GET_I_TUNES_SONGS action', () => {
    expect(generator.next().value).toEqual(
      takeLatest(homeContainerTypes.REQUEST_GET_I_TUNES_SONGS, getITunesSongs)
    );
  });

  it('should ensure that the action FAILURE_GET_I_TUNES_SONGS is dispatched when the api call fails', () => {
    const res = getITunesSongsGenerator.next().value;
    expect(res).toEqual(call(getSongs, artistName));
    const errorResponse = {
      errorMessage: 'There was an error while fetching repo informations.'
    };
    expect(
      getITunesSongsGenerator.next(apiResponseGenerator(false, errorResponse))
        .value
    ).toEqual(
      put({
        type: homeContainerTypes.FAILURE_GET_I_TUNES_SONGS,
        error: errorResponse
      })
    );
  });

  it('should ensure that the action SUCCESS_GET_I_TUNES_SONGS is dispatched when the api call succeeds', () => {
    getITunesSongsGenerator = getITunesSongs({ artistName });
    const res = getITunesSongsGenerator.next().value;
    expect(res).toEqual(call(getSongs, artistName));
    const songsResponse = {
      resultCount: 1,
      results: [{ artistName: artistName }]
    };
    expect(
      getITunesSongsGenerator.next(apiResponseGenerator(true, songsResponse))
        .value
    ).toEqual(
      put({
        type: homeContainerTypes.SUCCESS_GET_I_TUNES_SONGS,
        data: songsResponse
      })
    );
  });
});
