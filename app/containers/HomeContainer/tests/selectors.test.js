import { fromJS } from 'immutable';
import {
  selectHomeContainer,
  selectArtistName,
  selectSongsData,
  selectSongsError
} from '../selectors';

describe('HomeContainer selector tests', () => {
  let mockedState;
  let artistName;
  let songsData;
  let songsError;

  beforeEach(() => {
    artistName = 'sonu';
    songsData = { resultCount: 1, results: [{ artistName }] };
    songsError = 'There was some error while fetching the songs';

    mockedState = {
      homeContainer: fromJS({
        artistName,
        songsData,
        songsError
      })
    };
  });
  it('should select the homeContainer state', () => {
    const homeContainerSelector = selectHomeContainer();
    expect(homeContainerSelector(mockedState)).toEqual(
      mockedState.homeContainer.toJS()
    );
  });
  it('should select the artistName', () => {
    const artistSelector = selectArtistName();
    expect(artistSelector(mockedState)).toEqual(artistName);
  });

  it('should select songsData', () => {
    const songsDataSelector = selectSongsData();
    expect(songsDataSelector(mockedState)).toEqual(songsData);
  });

  it('should select the songsError', () => {
    const songsErrorSelector = selectSongsError();
    expect(songsErrorSelector(mockedState)).toEqual(songsError);
  });
});
