import { generateApiClient } from '@utils/apiUtils';
const songApi = generateApiClient('itune');

export const getSongs = repoName => songApi.get(`/search?term=${repoName}`);
